# TODO list Rest API
 
A simple TODO list Rest API application that allows users to manage their TODOs.
## Background Information - Architecture Design
The architecture design are strictly following the official standard from [spring boot architecture](https://spring.io/)

### Flow Architecture Design
The flow of the system is based on the standard spring boot flow architecture, leveraging the controller as listener to hosts the HTTP request and forward to service for the process action take place i.e. database insertion, logic handling user permission.

![img_6.png](documents/img_6.png)

### Security Architecture Design - Authentication & Authorization
For the security presumptive side is strictly follows the Spring security servlet application architecture. To enables both the authentication and authorization using authentication filter and authorization filter. It allows to block any unauthorized triffids into the application.
![img_5.png](documents/img_5.png)
## Getting Started

These instructions will give you a copy of the project up and running on
your local machine for development and testing purposes.

### Prerequisites

Requirements for the software and other tools to build, test and push
- [Spring-Boot](https://spring.io/guides/gs/spring-boot/)
- [MySql Server](https://www.mysql.com/)

### Installing
Before installation please take the time to look at prerequisites sections.
Once the necessary tool-kit has been installed the application. There are still several configuration requires to be made
![img_3.png](documents/img_10.png)

First, reconfigure the following line in the application.properties 
- MySQL Database url 
- MySQL Database username
- MySQL Database password

**Note** The pattern of the database url is as follows


    jdbc:mysql://{URL}:{port}/{database}


Secondly, maven installation for necessary dependency in the application to compiling

    mvn clean install 

or compiling with IntelliJ IDEA can simply click on the reload bottom appear on the top right on the screen when accessing pom.xml

![img_1.png](documents/img_7.png)
Finally, to initializing the application can use the maven command or via the IntelliJ IDEA by click on the Run TodoApplication.main()

    mvn spring-boot:run
![img_2.png](documents/img_9.png)

### Swagger Access

After the application is started and running in the port 8080 by completing above steps.

![img.png](documents/img.png)
The swagger ui are available for access at 

    http://localhost:8080/swagger-ui/

![img_1.png](documents/img_1.png)

**Noted** - Before testing the Todo-controller APIs, JWT token must be required, where this is obtainable from the POSTMAN Task Collections  
![img_2.png](documents/img_2.png)

The corresponding username & password which can be found in the TodoApplication.java 
![img_3.png](documents/img_3.png)

## Postman Collections
The [Postman Collections](documents/Task.postman_collection.json) is attached in the project root/document directory 

## Future Enhancement
- Role access management
- Real-time Collaboration
- Automation Development

## Authors

**Lau Hin Chak** - [GitLab](https://gitlab.com/HinChakLau)

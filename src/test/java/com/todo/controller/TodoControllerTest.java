package com.todo.controller;

import com.todo.model.Todo;
import com.todo.service.TodoService;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class TodoControllerTest {

    @InjectMocks
    TodoController todoController;

    @Mock
    TodoService todoService;

    @Test
    public void createToDo() {
        Todo todo = new Todo("Task", "DES", new Date(), "CREATED", 0L);
        when(todoService.createTodoTask(any(), any(Todo.class))).thenReturn(todo);
        Todo res = todoController.createTodoTask(null, todo);
        assertThat(res).isNotNull();
        assertThat(res.name).isEqualTo(todo.name);
    }

    @Test
    public void retrieveTodoList() {
        List<Todo> todoList = Collections.singletonList(new Todo("Task", "DES", new Date(), "CREATED", 0L));
        when(todoService.retrieveTodoList(any(), any(),  any(),  any())).thenReturn(todoList);
        List<Todo> res = todoController.retrieveTodoList(null, null, null, null);
        assertThat(res).isNotNull();
        assertThat(res.size()).isEqualTo(1);
    }

    @Test
    public void updateTodoTask() {
        Todo todo = new Todo("Task", "DES", new Date(), "UPDATE", 0L);
        when(todoService.updateTodoTask(any(), any(Todo.class))).thenReturn(todo);
        Todo res = todoController.updateTodoTask(null, todo);
        assertThat(res).isNotNull();
        assertThat(res.status).isEqualTo("UPDATE");
    }

    @Test
    public void deleteTodoTask() {
        when(todoService.deleteTodoTask(any(), any())).thenReturn(true);
        Boolean res = todoController.deleteTodoTask(null, 0L);
        assertThat(res).isNotNull();
        assertThat(res).isTrue();
    }
}

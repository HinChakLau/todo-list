package com.todo.service;

import com.todo.constant.Role;
import com.todo.model.User;
import com.todo.repository.UserRepository;
import com.todo.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl service;

    @Mock
    UserRepository repository;

    @Mock
    PasswordEncoder encoder;

    @Test
    public void loadUserByUsername() {
        User user = new User();
        user.setUserName("USERNAME");
        user.setPassword("PASSWORD");
        user.setRoles(Role.ADMIN);
        when(repository.findByUserName(any())).thenReturn(user);
        service.loadUserByUsername("TEST");
    }

}

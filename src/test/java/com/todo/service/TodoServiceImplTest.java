package com.todo.service;

import com.todo.constant.Role;
import com.todo.model.Todo;
import com.todo.model.TodoDTO;
import com.todo.model.User;
import com.todo.repository.TodoDTORepository;
import com.todo.repository.TodoRepository;
import com.todo.repository.UserRepository;
import com.todo.service.impl.TodoServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;


import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TodoServiceImplTest {

    @InjectMocks
    TodoServiceImpl todoService;

    @Mock
    ModelMapper modelMapper;

    @Mock
    UserRepository userRepository;

    @Mock
    TodoRepository todoRepository;

    @Mock
    TodoDTORepository todoDTORepository;

    @BeforeEach
    void setUp() throws Exception {
        modelMapper = mock(ModelMapper.class);
        MockitoAnnotations.initMocks(this);
    }

    User user = new User("TEST", "TEST", "TEST", Role.ADMIN);
    Authentication authentication = new Authentication() {
        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return null;
        }

        @Override
        public Object getCredentials() {
            return null;
        }

        @Override
        public Object getDetails() {
            return null;
        }

        @Override
        public Object getPrincipal() {
            return null;
        }

        @Override
        public boolean isAuthenticated() {
            return false;
        }

        @Override
        public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

        }

        @Override
        public String getName() {
            return "TEST";
        }
    };

    @Test
    public void createToDo() {
        user.setId(0L);
        when(userRepository.findByUserName(any())).thenReturn(user);
        when(todoRepository.save(any())).thenReturn(new Todo());
        Todo res = todoService.createTodoTask(authentication, new Todo());
        assertThat(res).isNotNull();
    }

    @Test
    public void retrieveTodoList() {
        when(todoDTORepository.findAll(any(Specification.class))).thenReturn(Collections.singletonList(new TodoDTO()));
        when(modelMapper.map(any(), any())).thenReturn(new Todo());
        List<Todo> res = todoService.retrieveTodoList(authentication, "name", "TEST", "");
        assertThat(res).isNotNull();
    }

    @Test
    public void updateTodoTask() {
        when(userRepository.findByUserName(any())).thenReturn(user);
        when(todoRepository.findByIdAndUserId(any(), any())).thenReturn(new Todo());
        when(todoRepository.save(any())).thenReturn(new Todo());
        Todo res = todoService.updateTodoTask(authentication, new Todo());
        assertThat(res).isNotNull();
    }

    @Test
    public void deleteTodoTask() {
        when(userRepository.findByUserName(any())).thenReturn(user);
        when(todoRepository.findByIdAndUserId(any(), any())).thenReturn(new Todo());
        doNothing().when(todoRepository).deleteById(any());
        when(todoRepository.findById(any())).thenReturn(Optional.empty());
        Boolean res = todoService.deleteTodoTask(authentication, 0L);
        assertThat(res).isNotNull();
        assertThat(res).isTrue();
    }

}

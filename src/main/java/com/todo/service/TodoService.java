package com.todo.service;

import com.todo.model.Todo;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface TodoService {

    Todo createTodoTask(Authentication authentication, Todo todo);

    List<Todo> retrieveTodoList(Authentication authentication, String filterWith, String filterBy, String sortedBy);

    Todo updateTodoTask(Authentication authentication, Todo todo);

    Boolean deleteTodoTask(Authentication authentication, Long id);
}

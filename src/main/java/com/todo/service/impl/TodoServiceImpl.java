package com.todo.service.impl;

import com.todo.expections.NoSuchEntityExists;
import com.todo.model.Todo;
import com.todo.model.TodoDTO;
import com.todo.model.User;
import com.todo.repository.TodoDTORepository;
import com.todo.repository.TodoRepository;
import com.todo.repository.TodoDTOSpecifications;
import com.todo.repository.UserRepository;
import com.todo.service.TodoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TodoRepository todoRepository;

    @Autowired
    TodoDTORepository todoDTORepository;

    @Override
    public Todo createTodoTask(Authentication authentication, Todo todo) {
        User user = userRepository.findByUserName(authentication.getName());
        todo.setUserId(user.id);
        todo.setName(user.getName());
        return todoRepository.save(todo);
    }

    @Override
    public List<Todo> retrieveTodoList(Authentication authentication, String filterWith, String filterBy, String sortedBy) {
        List<TodoDTO> res = todoDTORepository.findAll(TodoDTOSpecifications.customSearch(authentication.getName(), filterWith, filterBy, sortedBy));
        return res.stream().map(r -> modelMapper.map(r, Todo.class)).collect(Collectors.toList());
    }

    @Override
    public Todo updateTodoTask(Authentication authentication, Todo todo) {
        User user = userRepository.findByUserName(authentication.getName());
        if (todoRepository.findByIdAndUserId(todo.getId(), user.getId()) == null) throw new NoSuchEntityExists(todo);
        todo.setUserId(user.getId());
        return todoRepository.save(todo);
    }

    @Override
    public Boolean deleteTodoTask(Authentication authentication, Long id) {
        User user = userRepository.findByUserName(authentication.getName());
        if (todoRepository.findByIdAndUserId(id, user.getId()) == null) throw new NoSuchEntityExists(id);
        todoRepository.deleteById(id);
        return todoRepository.findById(id).isEmpty();
    }

}

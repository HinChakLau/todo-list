package com.todo.service;

import com.todo.model.User;

public interface UserService {

    User createUser(User user);

}

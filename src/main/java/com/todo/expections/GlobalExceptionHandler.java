package com.todo.expections;

import com.todo.constant.Message;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Global Exception Handler
 */
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoSuchEntityExists.class)
    ResponseEntity<Object> noSuchEntityExists(NoSuchEntityExists ex, WebRequest request) {
        return handleExceptionInternal(ex, Message.NoSuchEntityExists + ex.getResponseObject(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}

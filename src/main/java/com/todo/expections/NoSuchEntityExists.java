package com.todo.expections;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NoSuchEntityExists extends RuntimeException {

    public final String message = "NO SUCH ENTITY EXISTS";

    public Object responseObject;
}

package com.todo.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ModelMapper Configuration - For Object Mapping i.e. TodoDTO -> Todo
 */
@Configuration
public class ModelMapperConfiguration {

    @Bean
    public ModelMapper mapper () {
        return new ModelMapper();
    }

}

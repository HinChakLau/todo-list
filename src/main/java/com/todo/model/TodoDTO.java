package com.todo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Todo")
public class TodoDTO  extends BaseEntity {

    public String name;

    public String description;

    public Date dueDate;

    public String status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User user;

}
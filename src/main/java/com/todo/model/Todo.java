package com.todo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Todo extends BaseEntity {

    public String name;

    public String description;

    @JsonFormat(pattern="yyyy-MM-dd")
    public Date dueDate;

    public String status;

    @JsonIgnore
    @Column(name = "user_id")
    public Long userId;

}

package com.todo.repository;

import com.todo.model.TodoDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoDTORepository extends JpaRepository<TodoDTO, Long> , JpaSpecificationExecutor<TodoDTO> {

}

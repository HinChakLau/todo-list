package com.todo.repository;

import com.todo.model.TodoDTO;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public final class TodoDTOSpecifications {
    public static Specification<TodoDTO> customSearch(String name, String filterWith, String filterBy, String sortedBy) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            // Left Join USER in Todo entity
            root.fetch("user", JoinType.LEFT);
            predicates.add(criteriaBuilder.equal(root.get("user").get("userName"), name));
            // FILTER CONDITIONS
            if (filterWith != null && filterBy != null && validator(filterWith)) {
                try {
                    predicates.add(filterObject(criteriaBuilder, root, filterWith, filterBy));
                } catch (ParseException e) { System.out.println(e); }
            }
            // SORTING CONDITIONS
            if (sortedBy != null && validator(sortedBy)) query.orderBy(criteriaBuilder.desc(root.get(sortedBy)));
            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    private static boolean validator(String value) {
        return Arrays.stream(TodoDTO.class.getFields()).anyMatch(t -> t.getName().equals(value));
    }

    private static Predicate filterObject(CriteriaBuilder criteriaBuilder, Root root, String filterWith, String filterBy) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return filterWith.equals("dueDate") ?
                criteriaBuilder.lessThanOrEqualTo(root.<Date>get(filterWith), format.parse(filterBy)) :
                criteriaBuilder.equal(root.get(filterWith), filterBy);
    }

}

package com.todo;

import com.todo.constant.Role;
import com.todo.model.User;
import com.todo.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.mappers.ModelMapper;

import java.net.PasswordAuthentication;

@EnableWebMvc
@EnableJpaAuditing
@SpringBootApplication
public class TodoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoApplication.class, args);
	}

	@Bean
	CommandLineRunner run(UserService userService) {
		return args -> {
			userService.createUser(new User("Admin", "password", "Henry", Role.ADMIN));
			userService.createUser(new User("Jen", "password", "Jen", Role.USER));
		};
	}

	@Bean
	PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}


}

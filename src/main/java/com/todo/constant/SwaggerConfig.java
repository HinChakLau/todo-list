package com.todo.constant;

public class SwaggerConfig {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String JWT = "JWT";
    public static final String HEADER = "Authorization Header";

    public static final String GLOBAL = "global";

    public static final String ACCESS_EVERY_THINGS = "accessEverything";

    public static final String TITLE = "TODO REST API";

    public static final String DESCRIPTION = "Develop a simple TODO list Rest API application that allows users to manage their TODOs.";

    public static final String VERSION = "1.0.0";

}

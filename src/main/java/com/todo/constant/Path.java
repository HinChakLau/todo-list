package com.todo.constant;

public class Path {

    public static final String LOGIN = "/api/login";

    public static final String[] SWAGGER_URLS = {
            "/v2/api-docs",
            "/swagger-resources/**",
            "/swagger-ui/**",
            "/webjars/**"
    };


}

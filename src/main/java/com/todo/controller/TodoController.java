package com.todo.controller;

import com.todo.model.Todo;
import com.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * TODOs CRUD
 */
@RestController
@RequestMapping(path = "/api/todo")
public class TodoController {

    @Autowired
    public TodoService todoService;

    @PostMapping
    public Todo createTodoTask(@ApiIgnore Authentication authentication, @RequestBody Todo todo) {
        return todoService.createTodoTask(authentication, todo);
    }

    @GetMapping
    public List<Todo> retrieveTodoList(@ApiIgnore Authentication authentication,
                                       @RequestParam(required = false) String filterWith,
                                       @RequestParam(required = false) String filterBy,
                                       @RequestParam(required = false) String sortedBy) {
        return todoService.retrieveTodoList(authentication, filterWith, filterBy, sortedBy);
    }

    @PutMapping
    public Todo updateTodoTask(@ApiIgnore Authentication authentication, @RequestBody Todo todo) {
        return todoService.updateTodoTask(authentication, todo);
    }

    @DeleteMapping
    public Boolean deleteTodoTask(Authentication authentication, @RequestParam Long id) {
        return todoService.deleteTodoTask(authentication, id);
    }

}

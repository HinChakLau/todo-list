package com.todo.utility;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Configuration
public class JWTUtils {

    private static String secret = "secret";

    public static Algorithm getAlgorithm() {
        return Algorithm.HMAC256(secret.getBytes());
    }

    public static JWTCreator.Builder token(HttpServletRequest request, User user, Date expiresAt) {
        return JWT.create()
            .withSubject(user.getUsername())
            .withExpiresAt(expiresAt)
            .withIssuer(request.getRequestURL().toString());
    }

}
